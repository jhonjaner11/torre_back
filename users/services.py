import requests

def get_request(url, params={}):
    response = requests.get(url, params=params)

    if response.status_code == 200:
        return response.json()

def post_request(url, params={}):
    response = requests.post(url, params=params)

    if response.status_code == 200:
        return response.json()

#  Get user information by ID 
def get_user(user):
    response = get_request('https://torre.bio/api/bios/'+user)

    try:
        if response['person']:

            data = response
            user = {'person':data['person']}
            return data

    except TypeError:
        print(response)
        return {'msj':'Usuario no encontrado'}

#  Get skills details from the user 
def get_user_skill(user, skill):
    url = 'https://torre.co/api/genome/bios/'+user+'/strengths-skills/'+skill+'/detail'
    print (url)
    data = get_request(url)
    return data

# Get User Opportunities
def get_user_opportunities(user):
    params = {"bestfor":{"username":user}}
    url = 'https://search.torre.co/opportunities/_search?currency=USD%24&periodicity=hourly&lang=es&size=3&aggregate=true'
    data = post_request(url,params)
    return data

