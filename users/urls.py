from django.urls import path

from . import views

urlpatterns = [
    path('<user>', views.getUser, name='getUser'),
    path('<user>/skill/<skill>', views.getUserSkill, name='getUserSkill'),
    path('<user>/opportunities', views.getUserOpportunities, name='getUserOpportunities'),  
]
