from django.shortcuts import render
from django.http import HttpResponse, JsonResponse


# from requests.models import Response

from .services import get_user, get_user_skill, get_user_opportunities


#  Get user information by ID 
def getUser(request, user):
    data = get_user(user)
    return JsonResponse(data)

#  Get skills details from the user 
def getUserSkill(request, user, skill):
    data = get_user_skill(user, skill)
    return JsonResponse(data)
    
# Get User Opportunities
def getUserOpportunities(request, user):
    data = get_user_opportunities(user)
    return JsonResponse(data)
   
