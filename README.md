# Back Torre Application

En esta aplicacion, encontraras la forma en la que se consumen los servicios que brinda Torre.co
esta aplicacion hace parte del Test Tecnico para senior software developer vacancy at Torre.

Este software esta pensado en dos modulos, Usuarios y Oportunidades. 

This project was built with:
Frontside: Vue, Vuetify
Backside: Django, Django libreries (cors, json, etc)

Deployment Links in Heroku:

FRONT-END: https://front-torre.herokuapp.com/

BACK-END:  https://back-torre.herokuapp.com/


Project Links in Gitlab:

FRONT-END: https://gitlab.com/jhonjaner11/torre_front

BACK-END:   https://gitlab.com/jhonjaner11/torre_back

## Setup

The first thing to do is to clone the repository:

```sh
$ git clone https://gitlab.com/jhonjaner11/torre_back.git
$ cd torre_back
```

Create a virtual environment to install dependencies in and activate it:

```sh
$ virtualenv3 --no-site-packages env
$ source env/bin/activate
```

Then install the dependencies:

```sh
(env)$ pip install -r requirements.txt
```
Note the `(env)` in front of the prompt. This indicates that this terminal
session operates in a virtual environment set up by `virtualenv2`.

Once `pip` has finished downloading the dependencies:
```sh
(env)$ cd project
(env)$ python manage.py runserver
```
And navigate to `http://127.0.0.1:8000/`.
