import requests

def get_request(url, params={}):
    response = requests.get(url, params=params)

    if response.status_code == 200:
        return response.json()

def post_request(url, params={}):
    response = requests.post(url, params=params)

    if response.status_code == 200:
        return response.json()


# Get information about Opportunity
def get_opportunity(opportunity):
    url = 'https://torre.co/api/suite/opportunities/'+opportunity
    data = get_request(url)
    return data

# Gets the user's top 3 chances
def get_user_opportunities(user):
    params = {"bestfor":{"username":user}}
    url = 'https://search.torre.co/opportunities/_search?currency=USD%24&periodicity=hourly&lang=es&size=3&aggregate=true'
    data = post_request(url,params)
    return data

