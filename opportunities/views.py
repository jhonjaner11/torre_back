from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from .services import get_user_opportunities, get_opportunity
# from requests.models import Response

# Get information about Opportunity
def getOpportunity(request, opportunity):
    data = get_opportunity(opportunity)
    return JsonResponse(data)

# Gets the user's top 3 chances
def getUserOpportunities(request, user):
    data = get_user_opportunities(user)
    return JsonResponse(data)
