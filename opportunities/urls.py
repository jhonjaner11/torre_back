from django.urls import path

from . import views

urlpatterns = [
    path('<opportunity>', views.getOpportunity, name='getOpportunity'),
    path('<user>', views.getUserOpportunities, name='getUserOpportunities'),
    
]
